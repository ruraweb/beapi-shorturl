<?php
/*
 * Securité : Empêche d'accéder au fichier via une url
 */
if ( !defined( 'ABSPATH' ) ) {
	exit; // Le fichier ne peut être exécuté que dans l'environnement WP (afin d'éviter les attaques)
}

class BeApi_TinyUrlGenerator {
	/**
	 * Undocumented function
	 *
	 * @param [type] $url
	 * @return void
	 */
	public static function getTinyUrl( $url ) {
		$tinyurl = file_get_contents( 'http://tinyurl.com/api-create.php?url=' . $url );
		return $tinyurl;
	}

	/**
	 * Undocumented function
	 *
	 * @param [type] $post_id
	 * @param [type] $post
	 * @param [type] $update
	 * @return void
	 */
	public static function setTinyUrl( $new_status, $old_status, $post ) {
		if ( 'publish' === $new_status && 'publish' !== $old_status ) {
			$permalink = get_permalink( $post->ID );
			$tinyUrl   = self::getTinyUrl($permalink);

			update_post_meta($post->ID, 'tiny_url', $tinyUrl);
		}
	}

	/**
	 * Undocumented function
	 *
	 * @param [type] $columns
	 * @return void
	 */
	public static function beapi_addTinyUrlColumn( $columns ) {
		$columns['tiny_url'] = 'Tiny URL';
		return $columns;
	}

	/**
	 * Undocumented function
	 *
	 * @param [type] $column
	 * @param [type] $post_id
	 * @return void
	 */
	public static function beapi_displayTinyUrlColumn( $column, $post_id ) {
		if ( 'tiny_url' === $column ) {
			$tinyUrl = get_post_meta( $post_id, 'tiny_url', true );
			echo esc_html( $tinyUrl );
		}
	}
}

// Hooker la méthode setTinyUrl à l'action transition_post_status
add_action( 'transition_post_status', array('BeApi_TinyUrlGenerator', 'setTinyUrl'), 10, 3 );

// Ajouter la colonne custom dans l'edit panel
add_filter( 'manage_post_posts_columns', array( 'BeApi_TinyUrlGenerator', 'beapi_addTinyUrlColumn' ) );

// Afficher la meta Tiny URL dans la colonne custom
add_action( 'manage_post_posts_custom_column', array( 'BeApi_TinyUrlGenerator', 'beapi_displayTinyUrlColumn' ), 10, 2 );
