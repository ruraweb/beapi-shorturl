<?php
/*
Plugin Name: Be Api - Short-URL
Plugin URI:
Description: Tinify URL
Author: Be Api
Author URI:
Version: 1.0
*/

/*
 * Securité : Empêche d'accéder au fichier via une url
 */
if ( !defined( 'ABSPATH' ) ) {
  exit; // Le fichier ne peut être exécuté que dans l'environnement WP (afin d'éviter les attaques)
}

/*
 * Constantes du plugin
 */
if ( ! defined( 'BEAPI_SHORTURL_DIR' ) ) {
	define( 'BEAPI_SHORTURL_DIR', plugin_dir_path( __FILE__ ) ); // PATH du dossier du plugin
}

/*
 * Fichiers inclus
 */
require_once BEAPI_SHORTURL_DIR . 'src/beapi_tinyurlgenerator.php';
